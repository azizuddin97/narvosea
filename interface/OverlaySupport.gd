extends Node

# Thsi class represents an overlay support, helping out the overlay to update itself correctly. It contains the previous resources of the player
# used to show the difference in resources, the time since it last updated used so the overlay can stop showing the resource changes and the time since an item
# has been picked up used to display pick up messages on the overlay.
class_name OverlaySupport

# Stores the previous player resources to allow to calcluate how much the user has used.
var prev_resources: Dictionary setget set_previous_resources, get_previous_resources
# Both used to update the resource changes.
var last_update_time: float setget set_last_update_time, get_last_update_time
var changed:bool setget set_changed, get_changed
# Used to see how long it has been since an item has been picked up by the player.
var time_since_picked_up_item:float setget set_time_since_picked_up_item, get_time_since_picked_up_item

# The contructor for this class which sets up the previous resources all to 100, the time since picking up and item and last update to 0, and 
# a change happening to the resource values as false.
func _init()->void:
	prev_resources = {"Gold":100, "Gunpowder":100, "Repairs":100, "Crew":100, "Rum": 100}
	last_update_time = 0.0
	changed = false
	time_since_picked_up_item = 0.0

# Sets the time since the last update occured.
# @Param time:float -- The time it will be set to.
func set_last_update_time(time:float) -> void:
	last_update_time = time

# Gets the time of the last update.
# @Return last_update_time:float -- The time since last update.
func get_last_update_time() -> float:
	return last_update_time

# Sets the resource changed based on the parameter passed in.
# @Param changed_val:bool -- Whether the resource values have changed.
func set_changed(changed_val: bool) -> void:
	changed = changed_val

# Gets whether the resource values have changed.
# @Return changed:bool -- Whether the resource values have changed.
func get_changed() -> bool:
	return changed

# Set the previous resources dictionary with the dictionary resources past in.
# @Param resource:Dictionary -- The resources which the previous resources will be set to.
func set_previous_resources(resources: Dictionary) -> void:
	prev_resources = resources.duplicate(true)

# Get the dictionary prev_resources.
# prev_resources:Dictionary -- The previous resources the player had.
func get_previous_resources() -> Dictionary:
	return prev_resources

# Sets the time since the last pick up of an item
# @Param time:float -- The time it will be set to.
func set_time_since_picked_up_item(time:float):
	time_since_picked_up_item = time

# Gets the time since the last pick up of an item.
# @Return time_since_picked_up_item
func get_time_since_picked_up_item():
	return time_since_picked_up_item

