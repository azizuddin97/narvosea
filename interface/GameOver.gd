extends Control

# Controller for when the player has lost the game and corresponds the GameOver scene. Used to allow the player to return to the main menu when the game is over.

# Changes the current scene back to the main menu scene.
func _on_btnGameOver_pressed():
	assert(get_tree().change_scene("res://MainMenu.tscn") == OK)


func _on_btnCredits_pressed():
	get_tree().change_scene("res://Cutscenes/Credits.tscn")
