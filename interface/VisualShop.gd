extends Control

# This script is a controller between the visuals of the shop and the backend shop of the vendor the player is currently interacting with. This controlls
# the visuals shown on the screen for purchasing and selling items based on the backend shop instance of vendor the player is talking to and the 
# players items in which can be sold. It also updates the model when the player chooses to buy/sell things so that the game behaves as the player would expect.

# The instance of the game and shop they are currently in.
var playGame:PlayGame
var shop: Shop


# Called when the scene has loaded and sets up the initial state of the shop by using the game state and the current 
# shop the player is within from the backend of the game.
func _ready():
	#load playGame
	playGame = get_node("/root/Main").game
	shop = playGame.get_player().get_ship().get_current_island().get_vendor().get_shop()
	update()
	
	
# This method is used to update all the visuals for the shop including all the buyable and sellable items and their pictures
# names and prices along with updating the UI so it shows the player has spent money when purchasing or selling they have gained money.
# Useful for when a items has been sold and then entire inventory of the players needs to be shifted to reflect this.		
func update() -> void:
	#updates the overlay
	$Overlay.update()
	#assign upgrade names
	$Background/TextureRect/ShipUpgradeSlot/Name.text = shop.get_ship_upgrade_name()
	$Background/TextureRect/ItemSlot1/Name.text = shop.get_purchaseable_player_upgrade_name(0)
	$Background/TextureRect/ItemSlot2/Name.text = shop.get_purchaseable_player_upgrade_name(1)
	$Background/TextureRect/ItemSlot3/Name.text = shop.get_purchaseable_player_upgrade_name(2)
	#assign sellable items names
	$Background/PlayerInventory/ItemSlot1/Name.text = shop.get_item_name(0)
	$Background/PlayerInventory/ItemSlot2/Name.text = shop.get_item_name(1)
	$Background/PlayerInventory/ItemSlot3/Name.text = shop.get_item_name(2)
	$Background/PlayerInventory/ItemSlot4/Name.text = shop.get_item_name(3)
	$Background/PlayerInventory/ItemSlot5/Name.text = shop.get_item_name(4)
	$Background/PlayerInventory/ItemSlot6/Name.text = shop.get_item_name(5)
	#assign upgrade costs
	$Background/TextureRect/ShipUpgradeSlot/Cost.text = str(-shop.get_ship_upgrade_cost())
	$Background/TextureRect/ItemSlot1/Cost.text = str(-shop.get_player_upgrade_cost(0))
	$Background/TextureRect/ItemSlot2/Cost.text = str(-shop.get_player_upgrade_cost(1))
	$Background/TextureRect/ItemSlot3/Cost.text = str(-shop.get_player_upgrade_cost(2))
	#assign sellable item costs
	$Background/PlayerInventory/ItemSlot1/Cost.text = str(int(round(shop.get_player_item_cost(0))))
	$Background/PlayerInventory/ItemSlot2/Cost.text = str(int(round(shop.get_player_item_cost(1))))
	$Background/PlayerInventory/ItemSlot3/Cost.text = str(int(round(shop.get_player_item_cost(2))))
	$Background/PlayerInventory/ItemSlot4/Cost.text = str(int(round(shop.get_player_item_cost(3))))
	$Background/PlayerInventory/ItemSlot5/Cost.text = str(int(round(shop.get_player_item_cost(4))))
	$Background/PlayerInventory/ItemSlot6/Cost.text = str(int(round(shop.get_player_item_cost(5))))
	#assign resource costs
	$Background/TextureRect/RumSlot/Cost.text = str(shop.get_rum_cost())
	$Background/TextureRect/CrewSlot/Cost.text = str(shop.get_crew_cost())
	#assign upgrade images
	$Background/TextureRect/ShipUpgradeSlot/ShipUpgrade.texture_normal = load(get_image_path(shop.get_ship_upgrade_name()))
	$Background/TextureRect/ItemSlot1/Item.texture_normal = load(get_image_path(shop.get_purchaseable_player_upgrade_name(0)))
	$Background/TextureRect/ItemSlot2/Item.texture_normal = load(get_image_path(shop.get_purchaseable_player_upgrade_name(1)))
	$Background/TextureRect/ItemSlot3/Item.texture_normal = load(get_image_path(shop.get_purchaseable_player_upgrade_name(2)))
	#assign sellable item images
	$Background/PlayerInventory/ItemSlot1/Item.texture_normal = load(get_image_path(shop.get_item_name(0)))
	$Background/PlayerInventory/ItemSlot2/Item.texture_normal = load(get_image_path(shop.get_item_name(1)))
	$Background/PlayerInventory/ItemSlot3/Item.texture_normal = load(get_image_path(shop.get_item_name(2)))
	$Background/PlayerInventory/ItemSlot4/Item.texture_normal = load(get_image_path(shop.get_item_name(3)))
	$Background/PlayerInventory/ItemSlot5/Item.texture_normal = load(get_image_path(shop.get_item_name(4)))
	$Background/PlayerInventory/ItemSlot6/Item.texture_normal = load(get_image_path(shop.get_item_name(5)))
	
# Gets the path of the image for the item that needs to be displayed and returns it.
# @Param name:String -- The name of the item.
# @Return :String -- The path to the image.
func get_image_path(name: String) -> String:
	return "res://images/"+ name +".png"

# An upgrade item has been pressed on the screen therefore it updates the backend to buy to attempt to buy that item based on the item
# index and updates the overlay and the screen.
# @Param player_upgrade_index: int -- The index of the items thats being purchased.
func upgrade_item_pressed(player_upgrade_index: int)->void:
	# Updates the overlay
	shop.upgrade_item_purchase(player_upgrade_index)
	$Overlay.update()
	update()
	# Sets the mouse hover to exited .
	_on_Item_mouse_exited()

# Sells the item from the players items based on the index passed in by calling the backend shop method to do so.
# @Param index:int -- The position in the players items thats being sold.
func sell_item_pressed(index: int):
	shop.sell_item_pressed(index)
	# Updates the overlay
	$Overlay.update()
	update()
	

# The exit button has been pressed therefore the interaction is ended.
func onExitButton():
	var main = get_node("/root/Main")
	main.game.get_player().get_ship().get_current_island().end_special_npc_interaction()

# The ship upgrade button has been pressed therefore the backend shop is updated to reflect this.
func onShipUpgrade():
	shop.on_ship_upgrade()
	# Updates the overlay
	$Overlay.update()
	update()
	# The item has been purchased therefore the hover off method is called.
	_on_Item_mouse_exited()

# The first item button has been pressed therefore it buys the corresponding item.
func onBuyItem1Pressed():
	upgrade_item_pressed(0)

# The second item button has been pressed therefore it buys the corresponding item.
func onBuyItem2Pressed():
	upgrade_item_pressed(1)

# The third item button has been pressed therefore it buys the corresponding item.
func onBuyItem3Pressed():
	upgrade_item_pressed(2)

# The first item button has been pressed therefore it sell the corresponding item.
func onSellItem1Pressed():
	sell_item_pressed(0)

# The second item button has been pressed therefore it sell the corresponding item.
func onSellItem2Pressed():
	sell_item_pressed(1)

# The third item button has been pressed therefore it sell the corresponding item.
func onSellItem3Pressed():
	sell_item_pressed(2)

# The fourth item button has been pressed therefore it sell the corresponding item.
func onSellItem4Pressed():
	sell_item_pressed(3)

# The fifth item button has been pressed therefore it sell the corresponding item.
func onSellItem5Pressed():
	sell_item_pressed(4)

# The sixth item button has been pressed therefore it sell the corresponding item.
func onSellItem6Pressed():
	sell_item_pressed(5)

# The purchase crew button has been pressed therefore the backend shop is called to do so.
func onBuyCrewPressed():
	 # Buys the crew and updates the UI 
	shop.buy_crew()
	$Overlay.update()
	update()

# The purchase rum button has been pressed therefore the backend shop is called to do so.
func onBuyRumPressed():
	# Buys the rum and updates the UI 
	shop.buy_rum()
	$Overlay.update()
	update()

# Shows the items details i.e. the description of the item passed in
# @Param item:SellableItem -- The item in which their details will be displayed.
func show_item_details(item:SellableItem)->void:
	# If the item isnt null is shows the box and the details of the item.
	if (item != null):
		$ItemDetails.show()
		$ItemDetails/Description.text = item.get_description()

# When the first to buy is hovered over it shows its details to the player.
func _on_ItemSlot1_mouse_entered():
	var item:SellableItem = shop.get_purchaseable_player_upgrades()[0]
	show_item_details(item)

# When the secondto buy is hovered over it shows its details to the player.
func _on_ItemSlot2_mouse_entered():
	var item:SellableItem = shop.get_purchaseable_player_upgrades()[1]
	show_item_details(item)

# When the third to buy is hovered over it shows its details to the player.
func _on_ItemSlot3_mouse_entered():
	var item:SellableItem = shop.get_purchaseable_player_upgrades()[2]
	show_item_details(item)

# When the fourth to buy is hovered over it shows its details to the player.
func _on_ItemSlot4_mouse_entered():
	var item:SellableItem = shop.get_purchaseable_player_upgrades()[3]
	show_item_details(item)

# When the ship upgrade to buy is hovered over it shows its details to the player.
func _on_ShipUpgradeSlot_mouse_entered():
	var item:SellableItem = shop.get_ship_upgrade()
	show_item_details(item)

# When the mouse is no longer over the button, the details are hidden.
func _on_Item_mouse_exited():
	$ItemDetails.hide()
