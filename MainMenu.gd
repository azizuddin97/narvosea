extends Control

# Controller for the main menu setting up the relevant buttons in which each one corresponds to different actions occuring
# i.e. quit button quits the game and the new game sets up a new game.

func _ready():
	pass
# Called every cycle and plays the pirate song if it isnt already playing.
func _process(delta):
	if ($PirateSong.playing == false):
		$PirateSong.play(0.0)
# Creates a new instance of the game and sets the day night cycle to begin. It also changes the scene to the intro cutscene.
func _on_NewGame_pressed():
	Main.new_game_pressed = true
	DayNightCycle._on_start_game()
	get_tree().change_scene("res://Cutscenes/Intro.tscn")

# Loads the players resources from their recent save in their files and sets the players resources accoringly.
func load_player_resources():
	var save_game = File.new()
	# If they dont have a save file then returns.
	if not save_game.file_exists("user://save_resources.save"):
		return
	# Othwise it opens the file parses the results.
	save_game.open("user://save_resources.save", File.READ)
	var data = parse_json(save_game.get_as_text())
	save_game.close()
	# Generates the players resources from the data.
	get_node("/root/Main").game.player.resources = data['player_resources']

# Pressed to continue their saved game.
func _on_Continue_pressed():
	Main.new_game_pressed = false
	# Starts the time and sets up a new game then loads the relevant resources.
	DayNightCycle._on_start_game()
	get_node("/root/Main").new_game()
	load_player_resources()
	get_node("/root/Main").game.map.set_player_sector_and_island(2, 13)
	
# Quits the game completely.
func _on_Quit_pressed():
	get_tree().quit()

# Loads the game from current save.
func _on_load_button_pressed():
	load_data()

# Load the data for the game based on the users save file and sets all the games data accordingly.
func load_data():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return
	# Opens the file and parses the information from it.
	save_game.open("user://savegame.save", File.READ)
	var data = parse_json(save_game.get_as_text())
	save_game.close()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	# Gets the main and creates a new game with the data and starts the day night cycle.
	var main = get_node("/root/Main")
	DayNightCycle._on_start_game()
	main.new_game()
	main.view.update(data['state'], main.State,  data['island'], get_tree())
	main.game.current_state = data['state']
	

# Loads the current version of the game stored in the save text file and sets up the game 
# based on the data it contains.
func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
    # We need to revert the game state so we're not cloning objects
    # during loading. 
    #  We will accomplish this by deleting saveable objects.
	var save_nodes = get_tree().get_nodes_in_group("saveable")
	for i in save_nodes:
		i.queue_free()

    # Load the file line by line and process that dictionary to restore
    # the object it represents.
	save_game.open("user://savegame.save", File.READ)
	while save_game.get_position() < save_game.get_len():
        # Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())

        # Firstly, we need to create the object and add it to the tree and set its position.
		var new_object = load(node_data["filename"]).instance()
		get_node(node_data["parent"]).add_child(new_object)
		new_object.position = Vector2(node_data["pos_x"], node_data["pos_y"])

        # Now we set the remaining variables.
		for i in node_data.keys():
			if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
				continue
				new_object.set(i, node_data[i])
	save_game.close()