extends "res://addons/gut/test.gd"
var playerTest = preload ("res://player/Player.gd") #done

var player = playerTest.new()

#player class testing
func test_Player_resources_contructed_correctly():

	#getting the entire resource hashmap 
	var r = player.get_resources()

	#testing resources HashMap
	assert_eq(100, r["Gold"])
	assert_eq(100, r["Gunpowder"])
	assert_eq(100, r["Repairs"])
	assert_eq(100, r["Crew"])

	#testing get resource method to get specific resource:  player.get_resource_value(resource: String)
	assert_eq(100, player.get_resource_value("Gold"))
	assert_eq(100, player.get_resource_value("Gunpowder"))
	assert_eq(100, player.get_resource_value("Repairs"))
	assert_eq(100, player.get_resource_value("Crew"))


func test_Player_resource_change_amount():
	#testing change resource amount: player.change_resource_amount(key: String, value: int)
	player.change_resource_amount("Gold", 3)
	player.change_resource_amount("Gunpowder", 2)
	player.change_resource_amount("Repairs", 1)
	player.change_resource_amount("Crew", 4)

	#testing that resource hashmap is equal to the declared one 
	assert_eq(player.resources["Gold"], player.get_resource_value("Gold"))
	#print("Gold Value", player.get_resource_value("Gold"))
	assert_eq(player.resources["Gunpowder"], player.get_resource_value("Gunpowder"))
	#print("Gunpowder Value", player.get_resource_value("Gunpowder"))
	assert_eq(player.resources["Repairs"], player.get_resource_value("Repairs"))
	#print("Repairs Value", player.get_resource_value("Repairs"))
	assert_eq(player.resources["Crew"], player.get_resource_value("Crew"))
	#print("Crew Value", player.get_resource_value("Crew"))

func test_Player_ship():
	#testing get ship function: player.getship()
	assert_eq(player.ship, player.get_ship())


func test_Player_give_clue():
	#testing give clue function: palyer.give_clue(clue:Clue, sector_num:int) -> bool:
	assert_eq(true, player.give_clue( Clue.new("Navy", 1, "This is a test of a clue the first part represents the type of island the next clue is at, the second part is realted to the end game puzzle.")))
	#print("test give clue",player.give_clue( Clue.new("Navy", 1, "This is a test of a clue the first part represents the type of island the next clue is at, the second part is realted to the end game puzzle."), 3))

func test_Player_add_item():
	#testing add item on Array function: add_item(item:Item) -> bool:
	var item = Item.new("test_item_name", "test_description")
	player.add_item(item)
	assert_eq(true, player.add_item(item))

func test_Player_delete_item():
	var item = Item.new("test_item_name", "test_description")
	player.add_item(item)
	assert_eq(true, player.delete_item(1))

func test_Playere_get_index_of_item():
	#testing get index of item function: getIndexOfItem(item: Item) -> int:
	var item = Item.new("test_item_name", "test_description")
	player.add_item(item)
	assert_eq(2, player.getIndexOfItem(item))
