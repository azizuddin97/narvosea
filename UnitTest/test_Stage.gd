extends "res://addons/gut/test.gd"

var stageTest = preload("res://Encounter/Stage.gd") #done

var info = ["Setting:  Wooden_house",
"0|Description:  You have come face-to-face with a pirate what will you do?|Option:  1|Desc:  Speak to the pirate|Need_item:  PIRATE_LETTER|Option:  2|Desc:  Battle the pirate!|Need_item:  GEM|Give_item:  GEM|Special_interaction:  false",
"1|Description:  The pirate contains some important information about the mystical treaure|Option:  3|Desc:  Find out more?|Need_item:  null|Give_item:  null|Special_interaction:  false",
"2|Description:  The pirate runs away screaming for mercy. 'Please don't attack me just like that ye old Narvera Barsea|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  CLUE|Special_interaction:  false",
"3|Description:  Many have seeked out the mystical treasure and suspiciously have gone missing! I will join your crew and be part of the next 'Pirate Legends'.|null|Gold_change:  50|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  3|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false",
"4|Description:  Never gets here|null|"]

var all_stages = []

func test_constructor_description_options():
		#split lines into 2D components array
	for i in range(0, info.size()-1):
		all_stages.append(info[i].split("|"))
	var stage = stageTest.new(all_stages[1])
	assert_eq(stage.get_desc(), "You have come face-to-face with a pirate what will you do?")
	assert_eq(stage.get_options().hash(), {["1","Speak to the pirate"]: "PIRATE_LETTER",["2","Battle the pirate!"]: "GEM"}.hash())
