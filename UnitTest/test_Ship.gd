extends "res://addons/gut/test.gd"


var shipTest = preload ("res://player/Ship.gd")#done


var ship = shipTest.new()

#ship class testing
func test_Ship_speed():
	
	#testing get sheep speed 
	assert_eq(100, ship.get_speed())
	#setting speed 
	ship.set_speed(30)
	#testing get sheep speed with the defined values
	assert_eq(300, ship.get_speed())
	
func test_Ship_position():
	#testing get ship position 
	assert_eq(Vector2(), ship.get_position())
	#setting position 
	ship.set_position(Vector2(0,0))
	#testing get ship position 
	assert_eq(Vector2(), ship.get_position())
	
func test_Ship_target():
	#testing setting and getting target function
	var target = ship.set_target(Vector2(0,4))
	assert_eq(Vector2(0,4), ship.get_target())
	
func test_Ship_move_ship():
	#testing move_ship(delta) - moves the players ship towards the desired target location
	ship.set_target(Vector2(100,100))
	ship.set_speed(20)
	# Velocity is calculated by taking the working out the difference between the target and current ship position.
	var velocity = (ship.get_target() - ship.get_position())
	# The velocity is then normalized (so it is a unit vector) and multplied by the speed the ship will move at.
	velocity = velocity / velocity.length()
	velocity *= ship.get_speed()
	# The positon of the ship will then be equal to the current ships position + vector2 velocity scaled by the current time
	# Therefore the ship will move one unit vector closer each time the delta ticks.
	var position = ship.get_position() + velocity * 2
	ship.move_ship(2)
	assert_eq(velocity, ship.velocity)
	assert_eq(position, ship.get_position())



