extends "res://addons/gut/test.gd"

var item_test = load("res://map/items/Item.gd")
var item = item_test.new("test_item_name", "test_description")

var playerTest = preload ("res://player/Player.gd") #done
var player = playerTest.new()

func test_init():
	#testing _init(item_name: String, description: String): -> void
	#testing get_item_name() -> String:
	#testing get_description() -> String:
	assert_eq("test_item_name", item.get_item_name())
	assert_eq("test_description", item.get_description())

func test_interact():
	#testing interact(player)->bool:
	assert_eq(true, item.interact(player))
