extends "res://addons/gut/test.gd"

var mapTest = preload ("res://map/Map.gd")
var island = preload("res://map/islands/Island.gd")
var playerTest = preload("res://player/Player.gd")
var encounter_deck = FileReader.new().read("res://UnitTest/EncounterTest.txt")

var player = playerTest.new()
var map = mapTest.new(Vector2(0,0), player.get_ship(), player)

func test_create_sector():
#	testing create_sectors(player) -> void:
	
#	testing also sub method, position_islands(islands: Array, sector_centre: Vector2) -> void:
#	testing also sub method, connect_islands(sector: int, islands: Array) -> void:

	map.create_sectors(player)
	assert_eq(1, map.get_sector(1).size())
	assert_eq(14, map.get_sector(2).size())

func test_compute_island_type():
#	testing compute_island_type(island_index:int, sector: int)-> String:
	assert_eq("Navy", map.compute_island_type(1,2))
	assert_eq("Pirate", map.compute_island_type(2,2))
	assert_eq("Deserted", map.compute_island_type(4,2))
	assert_eq("Merchant", map.compute_island_type(5,2))
	assert_eq("Deserted", map.compute_island_type(10,2))

func test_navy_reached_player():
#	testing navy_reached_player() -> bool: [false]
	assert_false(map.navy_reached_player())
	
#	testing navy_reached_player() -> bool: [true]
	map.navy_ship.set_current_sector(2)
	map.player_ship.set_current_sector(1)
	map.navy_ship.set_position(Vector2(120,100))
	map.player_ship.set_position(Vector2(100,100))
	assert_true(map.navy_reached_player())

func test_should_move_navy():
#	testing should_move_navy() -> bool:
	assert_false(map.should_move_navy())

func test_is_visiting_island():
#	testing is_visiting_island(island) -> bool:
	var islandTest = preload("res://map/islands/Island.gd") #done
	var island = islandTest.new("Pirate", player)
	assert_false(map.is_visiting_island(island))

func test_move_player_ship():
#	testing move_player_ship(island: Island, next_sector: bool) -> bool:
	var islandTest = preload("res://map/islands/Island.gd") #done
	var island = islandTest.new("Pirate", player)
	assert_false(map.move_player_ship(island, true))
