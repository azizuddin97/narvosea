extends Node
# Used to read the text of a file based on the filepath passed in.
class_name FileReader

# Returns array of paragraphs (by splitting at double breaklines) which consists of the encounters 
# that exist in the game
# @Param filepath:String -- The filepath of the file to read from.
# @Return encounters:Array -- An array of the encounters information.
func read(filepath:String)->Array:
	#create file
	var file = File.new()
	#read file
	file.open(filepath, 1)
	var text = file.get_as_text()
	file.close()
	#split text into encounter paragraphs array
	var encounters = text.split("\n\n", false)
	#return paragraphs array
	return encounters