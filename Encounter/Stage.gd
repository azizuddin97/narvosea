extends Node

# A stage reprents a single stage of an encounter which includes the description of the stage, the options the stage
# has, any items and resources needed/ taken from the player and whether the stage has a special interaction 
# along with any save points/index.
class_name Stage
# The description
var desc = ""
# The options of the stage with the keys as an array of the option number and text and the value being the item/resource needed.
var options: Dictionary = {}
# The changes to resources the stage will cause.
var resource_changes= {"Gold":0, "Gunpowder":0, "Repairs":0, "Crew":0, "Rum":0, "Morale":0} 
# Keys of the resources.
var resource_keys = ["Gold", "Gunpowder", "Repairs", "Crew", "Rum", "Morale"]
# Item the player will get.
var give_item: Item setget set_give_item,get_give_item
# Whether the stage has a special interaction and should be saved.
var special_interaction: bool
var save_at_stage: bool


# Constructor for the stage. Creates the description of the stage and the possible options + the resource changes 
# the stage will lead to if it's a end node.
# @Param stage_info: Array -- Array containing all the information relevant to this particular stage of the encounter.
func _init(stage_info) -> void:
	desc = stage_info[1].split(":  ")[1]
	var i = 2
	# Loops through generating each option unless its a leaf node. If so, all info after null is resource changes.
	while i < stage_info.size():
		# Checks to see if the give_item section of the encounters text file has been reached.
		if stage_info[i].split(":  ")[0] == "Give_item":
			# If so it gets the name of the item and if its not null, gets the item from the list of story items in the StoryItemCollection class and places it into a var called give_item.
			var item_name = stage_info[i].split(":  ")[1]
			if (item_name != "null"):
				give_item = StoryItemCollection.new().get_story_item(item_name)
			i+=1
		# If the special interaction section has been reached and its not false in the encounter text file, it assigns the boolean of special_interaction to true so the NPC contaning the interaction
		# is able recognise it should trigger its special interaction if the player chooses this option.
		elif stage_info[i].split(":  ")[0] == "Special_interaction":
			if stage_info[i].split(":  ")[1] != "false":
				special_interaction = true
			i+=1
		# If the stage section has been reached and the corresponding value is not false, the stage should be saved in the encounter 
		# therefore it sets the save_at_stage variable to true and incremented to the next position.
		elif stage_info[i].split(":  ")[0] == "Save_encounter":
			if stage_info[i].split(":  ")[1] != "false":
				save_at_stage = true
			i+=1
		# If the index 2 is null, this means its currently parsing a non leaf branch of the text file and is at a part of the line which is the options, therefore
		elif stage_info[2] != "null":
			# Places the options into a dictionary where the option number and the option description is the key as an array and then value is the item needed to pick that option.
			options[[stage_info[i].split(":  ")[1], stage_info[i+1].split(":  ")[1]]] = stage_info[i+2].split(":  ")[1]
			i+=3
		# Othwise its a branch node and the rest of the line conists of resources change names with corresponding values.
		else:
			if i != 2: 
				resource_changes[resource_keys[i-3]] = stage_info[i].split(":  ")[1]
			i += 1


# Gets the current description of the stage.
# @Return desc:String -- The description of this stage of the encounter	
func get_desc() -> String:
	return desc

# Gets all the options this stage has to chose from.	
# @Return options:Dictionary -- The options of this stage of the encounter	
func get_options() -> Dictionary:
	return options

# Gets the item (if any) the current stage gives the player.
# @Return give_item:Item -- The item the stage will give the player.
func get_give_item() -> Item:
	return give_item
	
# Sets the item that the stage will give the player.
# @Param item:Item -- The item that the stage will give the player.
func set_give_item(item:Item)->void:
	give_item = item
	
# Returns true if this stage of the encounter has a special interaction
# @Return special_interaction:bool -- Whether the stage has a special interaction.
func has_special_interaction()->bool:
	return special_interaction

# Returns true if this stage of the encounter should be saved.
# @Return save_at_stage:bool -- Whether the stage should be saved.
func should_save_at_stage() -> bool:
	return save_at_stage
