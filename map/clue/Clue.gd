extends "res://map/items/Item.gd"

# A clue is a special type of item relating to clues in order to gain the treasure at the end of the game.
class_name Clue

# Refers to the island type that the next clue will be on.
var next_island_type: String setget ,get_next_island_type
# Refers to the option at the end of the game is correct i.e. 1  means the first choice out of the player's options.
var end_game_option: int setget ,get_end_game_option

# Constructs the Clue based on the island type of the next clue, the end game option and some text.
# @Param tmp_island_type:String -- The type of island the next clue is on
# @Param tmp_end_option:int -- The option to chose at the end game obstacle
# @Param tmp_clue_text:String -- The text the clue contains.
func _init(tmp_island_type:String, tmp_end_option:int, tmp_clue_text:String).("Clue", tmp_clue_text)-> void:
	next_island_type = tmp_island_type
	end_game_option = tmp_end_option
	
# Overrides the parent class implementation by giving the player a clue instead of an item and 
# changing the players state to picked up a clue.
# @Param player:Player -- The player instance
# @Param :bool -- Whether the clue was added.
func interact(player)->bool:
	player.give_clue(self)
	player.current_state = player.Player_states.PICKED_UP_CLUE
	return true
	
# Gets the next clues island type.
# @Return next_island_type:String -- The type of island the next clue is at.
func get_next_island_type() -> String:
	return next_island_type

# Gets the correct option for the end game obstacle.
# @Return end_game_option:int -- The end game option it corresponds to.
func get_end_game_option() -> int:
	return end_game_option

# Whether this clue is equal to another object
# @Param o:Node -- A node object.
# @Return :bool - Whether the two are equal.
func equals(o: Node) -> bool: 
	if o as Clue:
		var clue: Clue = o as Clue
		if get_description() != clue.get_description():
			return false
		if next_island_type != clue.get_next_island_type():
			return false
		if end_game_option != clue.get_end_game_option():
			return false
		return true
	return false

# Returns a string representation of the Clue.
# @Return :String -- A string representation of the clue.
func to_string() -> String:
	return ("The next island type is: " + 
		next_island_type + ".\nThe end game option is " + 
		(end_game_option as String) + ".\nThe clue text is " + get_description())