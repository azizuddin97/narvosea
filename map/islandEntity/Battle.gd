extends "res://map/islandEntity/SpecialInteraction.gd"

# A battle represents a battle between the player and a NPC which contains several rounds until either the player is out of crew or if the 
# opponent runs out of health. The acts like a rock paper scissors style event with the three sword attacks but then also 
# the player can shoot or use an explosive barrel on the NPC. The NPC also has a corresponding difficulty level.

class_name Battle

# The enemy crew count and action.
var enemy_crew: int
var enemy_action: String
# The players action and array of acceptable actions.
var player_action:String
var actions = ["Light Attack", "Heavy Attack", "Parry", "Shoot", "Explosive Barrel"]
# The difficulty of the battle and who won as well as whether a round actually was played i.e. the player choose a valid option.
var difficulty: int
var who_won:String
var round_played:bool

# Contructor which creates a battle using the player instance passed in and a difficulty of the battle.
func _init(player, temp_difficulty:int).(player):
	#seed rng
	randomize()
	#set enemy health based on its difficulty.
	enemy_crew = 80 + (20 * (temp_difficulty/2))
	difficulty = temp_difficulty
	
# This method is called when the player decides to shoot their gun in which returns an amount of damage 1 if they have a gun and another
# gunpowder in order to shoot their gun. It gets the highest level gun to shoot from and then decrements its uses by 1 removing the gun
# if they only had one shot. Othewise it retursn 0 meaning the player couldnt shoot.
# @Return :int -- Whether the player shoot or not.
func shoot() -> int:
	# The items the player has.
	var items := player.get_items()
	var gun :Item = null
	#for each gun
	for item in items:
		if item != null && item.get_item_name() == "Gun":
			#if item is weaker gun than current one, use weaker one
			if (gun == null) or (item.get_item_level() < gun.get_item_level()):
				gun = item
	# If the player hasn't got a gun or another gunpowder.
	if gun == null || player.adjustResource("Gunpowder" ,player.get_resource_value("Gunpowder")) < 20:
		#player hadn't a gun
		return 0
	# Othewise gunpowder is taken from the player.
	player.change_resource_amount("Gunpowder", -20)
	#delete the gun from player
	player.delete_item(player.getIndexOfItem(gun))
	if gun.get_item_level() > 1:
		#put back gun, minus 1 level
		gun.set_item_level(gun.get_item_level() -1)
		player.add_item(gun)
	#player had a gun
	return 1

# The player has chosen to use a powder barrel therefore if they have one it returns 1 indicating that they succesfully used this attack and removes it from their inventory
# otherwise it returns 0. 
# @Return :int -- Whether they succesfully used the barrel.
func powder_barrel() -> int:
	var items = player.get_items()
	#find explosive barrel in inventory
	for item in items:
		if item != null && item.get_item_name() == "Explosive Barrel":
			#if found, delete it and return 1
			player.delete_item(player.getIndexOfItem(item))
			return 1
	#not found, do nothing and return 0
	return 0

# Calcuates how much damage to do to the enemy based on their and the players action. i.e. if they won, drew or lost the round then
# they do a different amount of damage.
# @Return :int -- The damage that should be dealt to the enemy.
func damage_to_enemy(player_action: String, enemy_action: String) -> int:
	#these have flat values
	if (player_action == "Shoot"):
		return 100 * shoot()
	elif (player_action == "Explosive Barrel"):
		return 200 * powder_barrel()
	#these are rock paper scissors
	#if same
	elif (player_action == enemy_action):
		return 10
	#if player wins
	elif ((player_action == "Light Attack") and (enemy_action == "Heavy Attack")) or ((player_action == "Heavy Attack") and (enemy_action == "Parry")) or ((player_action == "Parry") and (enemy_action == "Light Attack")):
		 return 30
	#otherwise enemy wins
	return 0

# Calcuates how much damage to do to the player based on their and the enemys action. i.e. if they won, drew or lost the round then
# they do a different amount of damage.
# @Return :int -- The damage that should be dealt to the player.
func damage_to_player(player_action: String, enemy_action: String) -> int:
	if (player_action == "Shoot") || (player_action == "Explosive Barrel"):
		return 0
	#these are rock paper scissors
	#if same
	if (player_action == enemy_action):
		return 4
	#if player wins
	if ((player_action == "Light Attack") and (enemy_action == "Heavy Attack")) or ((player_action == "Heavy Attack") and (enemy_action == "Parry")) or ((player_action == "Parry") and (enemy_action == "Light Attack")):
		 return 0
	#otherwise enemy wins
	return 6

# Update the variable who_won, if the player took no damange, they won, if they took 4 they drew, otherwise they lost.
# @Param dmg:int -- The amount of damage the player took
# @Return String -- Who won the round.
func get_who_won(player_damage:int)->String:
	if player_damage == 0:
		return "Player"
	elif player_damage == 6:
		return "Enemy"
	else:
		return "Draw"

# Updates whether a round was actually played
# @Param player_dmg:int -- The damage to the player
# @Param enemy_dmg:int -- The damage to the enemey.
func update_round_played(player_dmg:int, enemy_dmg:int)->void:
	# If they both took no damage then a round wasn't played.
	if player_dmg == 0 && enemy_dmg == 0:
		round_played = false
	else:
		round_played = true

# Applies the player equipped sword bonus damage to the inital damage amount.
# @Param dmg:int -- The base damage amount.
# @Return dmg + (dmg * float(sword.get_item_level())/10) -- The final damage amount.
func apply_sword_boost(dmg:int)->float:
	#get sword from player
	var sword: UseableItem = player.get_equipped_sword()
	#if no sword, or no damage in the first place, do not boost
	if (sword == null || dmg == 0):
		return float(dmg)
	#add 10% damage boost per sword level
	return dmg + (dmg * float(sword.get_item_level())/10)

	
# Simulates the next round of the battle based on the action the player does
# including updating both the player and the enemys crew/health values, the items for the player if they used one
# and calucates who won the round
# @Param player_action:String -- The players action.
func next_round(player_action: String) -> void:
	#seed rng
	randomize()
	self.player_action = player_action
	#enemy action is one of the first 3 actions (parry, light attack, heavy attack)
	enemy_action = actions[randi()%3]
	#get damage dealt to enemy
	var enemy_dmg: int = damage_to_enemy(player_action, enemy_action)
	#debugging print 
	print(int(apply_sword_boost(enemy_dmg) -  (enemy_dmg * float(difficulty)/10)))
	#boost sword damage, then reduce by 10% for each difficulty point
	var enemy_crew_dmg:int = int(apply_sword_boost(enemy_dmg) -  (enemy_dmg * float(difficulty)/10))
	#boost to the minimum of 5 damage (if there is any)
	if enemy_crew_dmg < 5 && enemy_dmg != 0:
		enemy_crew_dmg = 5
	#apply damage to enemy
	enemy_crew -= enemy_crew_dmg
	#get damage to player
	var player_dmg = damage_to_player(player_action, enemy_action)
	#apply damage to player (boosted by 10% per difficulty point)
	player.change_resource_amount("Crew", (int(-player_dmg * float(difficulty)/10) - player_dmg ))
	update_round_played(player_dmg, enemy_dmg)
	# Update the variable who_won, if the player took no damange, they won, if they took 4 they drew, otherwise they lost.
	who_won = get_who_won(player_dmg)
	
	
# Triggers the special interaction so the player enters the battle state
func trigger()->void:
	player.change_state(player.Player_states.BATTLING)
