extends Node

# A special interaction is an abstract class in which NPCs have that will trigger something unique to happen in the encounter.
class_name SpecialInteraction
# The player being interacted with.
var player:Player

# Contructs the interaction with the player passed in.
# @Param player:Player -- An instance of player in which is going to interact
func _init(player)->void:
	self.player = player

# What happens when the interaction is triggered
# Must be overridden by child classes.
func trigger()->void:
	return
# Changes the state of the player back to its state before.	
func finish_interaction()->void:
	player.change_state(player.check_state)
