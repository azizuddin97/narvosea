extends "res://map/islandEntity/NPC.gd"

# A moveable NPC is an NPC which moves around the island and can be aggresive towards the player (meaning they will attempt to chase the player).
class_name MoveableNPC

# Whether the NPC will be aggressive
var is_opressive: bool
# The default position of the NPC.
var default_position: Vector2 setget ,get_default_position
# The different states the NPC can be in
enum STATE {
	TRIGGERED,
	NOT_TRIGGERED
}
# Which state the player is in.
var triggered: int

# Constructs the NPC including their position, encounter, type, difficulty and whether they are aggressive.
# @Param tmp_encounter: Array -- The encounter information to contruct the NPCs encounter.
# @Param pos: Vector2 -- The position of the NPC.
# @Param temp_type: String -- The type the NPC is.
# @Param aggresive:bool -- Whether the NPC is aggressive.
# @Param player:Player -- The player that will interact with the NPC.
# @Param difficulty:int -- The difficulty of the NPC.
func _init(tmp_encounter: Array, pos: Vector2, temp_type: String, aggresive:bool, player:Player, difficulty:int).(tmp_encounter, pos, temp_type, player, difficulty):
	position = Vector2(10700,10300)
	default_position = position
	is_opressive = aggresive

# Updates the NPC target position and state to triggered if close to the player, otherwise sets its state to NOT TRIGGERED and either stands still
# if near the player and not aggressive otherwise chases the player.
# @Param player_character:PlayerCharacter -- The players character.
func move_entity(player_character: PlayerCharacter):
	# If the NPC is aggressive and close to the player then the NPC can be triggered.
	if(is_opressive && position.distance_to(player_character.get_position()) < 250):
		is_triggered(player_character.get_position())
	# Or if the NPC is not aggressive and very close to the player than they will be triggered to perform differently,
	elif (!is_opressive && position.distance_to(player_character.get_position()) < 150):
		is_triggered(player_character.get_position())
	# Othewise they aren't triggered.
	else:
		triggered=STATE.NOT_TRIGGERED

# Updates the NPCs state and target position to the position passed if the NPC is aggressive or sets its target to its current position
# if the NPC is passive.
# @Param player_positio:Vector2 -- The position of the players character.
func is_triggered(player_position: Vector2):
	# Sets the NPC to triggered
	triggered = STATE.TRIGGERED
	# If the NPC is aggressive then its target is the players position
	if is_opressive:
		target=player_position
	# Otherwise they stand still.
	else:
		target = position
		

# Gets the NPCs default position.
# @Return default_position:Vector2 -- The NPCs default position.
func get_default_position() -> Vector2:
	return default_position
	
# Sets the NPCs target position
# @Param pos:Vector2 -- The new target position.
func set_target(pos: Vector2) -> void:
	target = pos

# Gets the NPCs target position
# @Return target:Vector2 -- The NPCs target position.
func get_target() -> Vector2:
	return target
