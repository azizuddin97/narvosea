extends "res://map/items/Item.gd"

# Class extending item, representing any item in which can be sold for a gold value.
class_name SellableItem

# The amount the items is worth in gold and the types of Sellable only items.
var gold_value: int setget set_gold_value, get_gold_value

# Constructs a sellable item with the name descripton and gold value.
# @Param item_name: String -- The name of the item
# @Param description: String -- The description of the item
# @Param gold_amount: int -- The items gold value.
func _init(item_name: String, description: String, gold_amount: int).(item_name, description):
	#self._setUp(item_name, description)
	gold_value = gold_amount

# Get the gold value of the item
# @Return gold_value:int
func get_gold_value() -> int:
	return gold_value

# Sets the gold value
# @Param gold:int -- The gold value.
func set_gold_value(gold: int) -> void:
	gold_value = gold
