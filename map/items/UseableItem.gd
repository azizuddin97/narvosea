extends "res://map/items/SellableItem.gd"

# Class for any useable item within battles and has a level in determines its number of uses.
class_name UseableItem

# The level of the item.
var item_level: int setget ,get_item_level #decrement this when item is used

# Constructs the useable item based off the name and level passed in.
# @Param item_name: String -- The name of the item.
# @Param level_cap:int -- The level of the item.
func _init(item_name: String, level_cap:int).("","",0):
	item_level = level_cap
	var value:int = 20
	# Works out the cost of items based on their name.
	if item_name == "Explosive Barrel":
		value = 200
	elif item_name == "Gun":
		value = 150
		# Gets the rest of the item details
	var desc: String = create_item_details(item_name)
	# Calls the parent constructor.
	._init(item_name, desc, value)
	
# Creates the items description and alters its level depending on its name i.e. an Explosive Barrel can only be used once therefore always level 1
# @Param item_name:String -- The name of the item
# @Return desc:String -- The description the item should be set with.
func create_item_details(item_name:String)->String:
	var desc:String
	# If the item is a gunpowder barrel it doesn't level and is one time use therefore set its level to 1 and a relevant description
	if (item_name == "Explosive Barrel"):
		desc = "This Explosive Barrel can do a great deal of damage to your opponent but can only be used once."
		item_level = 1
	# If the item is a sword, then the description will tell the player about how the level affects the damage.
	elif (item_name == "Sword"):
		desc = "Sword level: " + item_level as String + ". The level of the Sword impacts the damage dealt to opponents when in battle."
	# Otherwise it tells the player the item has a certain amount of uses.
	else:
		desc = item_name + " level: " + item_level as String + ". This item can be used " + item_level as String + " times."
	return desc

# Gets the level of the item
# @Return item_level:int  -- The level of the item.
func get_item_level() -> int:
	return item_level

# Sets the level of the item
# @Param new_item_level:int -- The new level the item will be set to.
func set_item_level(new_item_level: int):
	item_level = new_item_level
	
func use_item() -> void:
	pass 
	
	
	
