extends "res://CharacterTopDownView/NPCs/Navigation.gd"
# The canvas modulate and the light for the character are stored in corresponding variables.
onready var cm = $CM
onready var light = $LT
# Whether the scene has loads and the lights are currently on.
var loaded:bool
var lights_on = false

# Start the processing when entering scene, turning off the lights and setting up the character accordingly 
# including setting up the corresponding character with the players character of the backend and sets 
# the position based the backend player character position.
func _ready():
	# Turns the light off.
	turn_lights_off()
	# Load the stored character position
	character = get_node("/root/Main").game.get_player().get_character()
	# If the players position has been updated it uses the players updated position
	if Main.updated_player_position:
		position = Main.updated_player_position
	# Otherwise it usese the character current position.
	else:
		position = character.get_position()
		# Sets the final target to the current position and sets up the speed and whether its loaded.
	final_target = position
	speed = 200 
	loaded = false
	set_physics_process(true)

# Updates the player characters positon and the light based on the backend players position and 
# the time of day.
func _physics_process(delta):
	# If the players position has been updated is uses the updated position.
	if Main.updated_player_position:
		position = Main.updated_player_position
		Main.updated_player_position = null
	# Gets the seconds and mins of the day night cycle.
	var seconds = int(DayNightCycle.time) % 60
	var minutes = int(DayNightCycle.time) / 60
	# If the minutes is more than 6 it updates the seconds to check whether the light needs turning on.
	if minutes > 6:
		seconds = seconds + (minutes * 60)
	# If the seconds is greater than 455 seconds the time is night therefore if the lights aren't on they are turned on.
	if seconds >= 455:
		if not lights_on:
			turn_lights_on()
	# Otherwise if the lights are on they are turned off.
	else:
		if lights_on:
			turn_lights_off()
			
# Turns the lights on including the canvas modulate and sets the boolean to lights being on.
func turn_lights_on()->void:
	cm.visible = true
	light.visible = true
	lights_on = true

# Turns the lights off including the canvas modulate and sets the boolean to lights being off.
func turn_lights_off():
	cm.visible = false
	light.visible = false
	lights_on = false
	
# Draws a dotted line if the player is going somewhere from the current position to the target.
func _draw()->void:
	#if going somewhere
	if going_somewhere():
		#for every point in the path
		for point in path:
			#draw a dot at the point
			draw_circle(point - get_global_position(), 8, Color(1, 0, 0))

# If the user left clicks on the screen, the entities target position is the mouse position.
func _unhandled_input(event):
	#if not a mouseclick
	if not event is InputEventMouseButton or not InputEventMouseMotion:
		#don't care
		return	
	#if not a left mouseclick press
	if event.button_index != BUTTON_LEFT or not event.pressed:
		#don't care
		return
	# If the scene that contains this hasn't fully loaded.
	if !loaded:
		return
	#when mouse clicked
	#target is where the mouseclick happened
	var target = get_global_mouse_position()
	#player node is under the tilemap
	#set the player's navigation target
	set_final_target(target)
	character.set_target(target)
	
	
	
	
	
	