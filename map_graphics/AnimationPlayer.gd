extends AnimationPlayer

# Plays the day night cycle.
func init(): 
	play("DayAndNight")
	seek(100,true)
