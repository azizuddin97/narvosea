extends Area2D

# This script is the controller for the visual islands in which when clicked sends a signal to whether this is used that an island has been clicked. 

signal island_clicked


func _ready():
	pass

#when island clicked
func _on_IslandGraphic_input_event(viewport, event, shape_idx):
	if event is InputEventScreenTouch and event.pressed:
		#emit signal to containing script
		emit_signal("island_clicked", self, event)

#when mouse over island
func _on_IslandGraphic_mouse_entered():
	#emit signal for other scripts
	emit_signal("island_hovered")

