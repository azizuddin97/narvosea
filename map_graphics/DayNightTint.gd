extends Node2D

# This script respresents the day night tint that happens to the map and islands.

# When the scene has loaded if the time has started it plays the day night cycle tint over the screen.
func _ready():
	var time = DayNightCycle.get_time()
	var animation_player = $AnimationPlayer
	# If the day night cycle has started then it plays the day night cycle and skips to the correct 
	# part
	if(DayNightCycle.get_start()==true):
		animation_player.play("day_and_night")
		animation_player.seek(time,false)
	

