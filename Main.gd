extends Node

# The main controller for the game, creating an instance of PlayGame and tells the view (controller) to update based on the 
# state of the game itself. It also coordinates updating the model/Game itself.
const main_menu_script = preload("res://MainMenu.tscn")
# An instance of PlayGame and View used to coordinate displaying the views based on the models state.
var game: PlayGame
var view: View
# The states of the game and the current state and previous state used to update the view.
enum State {MAIN_MENU, MAP, ISLAND, ENCOUNTER, PAUSED, INVENTORY, WIN, GAME_OVER, CHARACTER_SELECT, SHOP, ENDGAME, BATTLE}
var current_state:int
var previous_state:int
# Whether the players position has been updated or if the new game has been preseed.
var updated_player_position = null
var new_game_pressed = false
# The player instance.
var player
# Waits till all nodes are loaded in then contructs the map and the player which will be constants throughout the game.
func _ready() -> void:
	view = View.new()
	current_state = State.MAIN_MENU
	view.update(current_state, State, null, get_tree())
	

# Sets up a new game and switches the current state to the MAP, thererfore the view will update to match that current state.
func new_game():
	game = PlayGame.new()
	player = game.player
	# Sets the states to MAP.
	current_state = State.MAP
	previous_state = State.MAP
	# Updates the view.
	view.update(current_state, State, null, get_tree())
	view.overlay_support = OverlaySupport.new()

# Contains the main logic loop and if the state of the game is not the inventory or paused menu, it updates the previous
# state to be the current (it doesnt for these as the player needs to return to the previous state if exiting either of these).
# @Param delta:float -- The delta time.
func _process(delta:float):
	if (game != null):
		# If the current state in Main does not match the state of the game, then the games state has updated.
		if (current_state != game.current_state):
			# Updates the previous state so it can return back to it when it needs (apart from for the inventory and pause menus.)
			if (current_state != State.INVENTORY && current_state != State.PAUSED):
				previous_state = current_state
			# Sets the current state to the same as that games current state.
			current_state = game.current_state
			view.update(game.current_state, State, game.player.get_ship().get_current_island(), get_tree())
		# Calls the game process method.
		death_reason()
		game._process(delta)

# Saves the game including the current state and island if the current state is on an island.
# @Return dict:Dictionary -- A dictionary of save data.
func save()->Dictionary:
	var island = null
	# If the current state is on an island then it gets the current island.
	if (current_state  == State.Island):
		island =  game.player.get_ship().get_current_island()
	# Crates a dictionary with the save data and rturns it
	var dict = {
		"filename": get_filename(),
		"parent": get_parent().get_path(), 
		"current_state": current_state,
		"island": island
	}
	return dict

# The players death reason is passed on to the view class which manages the scenes:
func death_reason():
		# If no gold then the reason is no gold
		if player.get_resource_value("Gold") < 0:
			view.death_reason = "NoGold"
		# If no crew and was in a battle then they dided to a fight.
		elif player.get_resource_value("Crew") < 1 && current_state == State.BATTLE:
			view.death_reason = "Fight"
		# Otherwise they just lost their crew.
		elif player.get_resource_value("Crew") < 1:
			view.death_reason = "NoCrew"
		# If no rum left then they died as have no rum.
		elif player.get_resource_value("Rum") < 0:
			view.death_reason = "NoRum"
		# If they had no repairs then they died due to lack of repairs.
		elif player.get_resource_value("Repairs") < 0:
			view.death_reason = "NoRepairs"
